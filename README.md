# POC - Getup Cloud

Projeto criado para o processo seletivo da Getup :)

## Componentes

Esse projeto utiliza o terraform para prover uma infraestrutura como código e imutável.
Seu state está disponível aqui no repositório, mas o projeto contempla o uso de um bucket no GCP.

Os arquivos tf implementam os seguintes itens:

### A Nível de Projeto/Infra

-   Um Projeto na GCP (Google Cloud Platform) chamado getup-teste;
-   Uma VPC Network, com as Subnets Necessárias para a execução de um Cluster k8s nativo de VPC;
-   Um Cluster GKE (Google Kubernetes Enginer), na versão 1.13.6, um Cluster Kubernetes Gerênciado na GCP (Google Cloud Platform);
-   Um bucket para o armazenamento do terraform.tfstate
-   Ativação das APIs no GCP necessárias para o projeto em questão.

### A Nível do Cluster k8s

-   Tiller e permissões (RBAC) para a utilização do Helm
-   Nginx Ingress Controller (External Load Balancer)
-   Prometheus Operator, para a implementação do Prometheus, o Alertmanager e o Grafana

### Disclaimers

-   O GKE tem a disposição Clusters Regionais, altamente recomendados para um Cluster k8s altamente disponível, mas não o fiz para não gerar um consumo alto de créditos;
-   O GKE faz, por default, uso da tool [prometheus-to-sd](https://github.com/GoogleCloudPlatform/k8s-stackdriver/tree/master/prometheus-to-sd) desde o GKE 1.12, com a finalidade de exportar dados para o Stackdrivers e não ser necessária a instalação do Prometheus Server. Desabilitei esse recurso dada a finalidade desta avaliação :)
-   Prometheus Operator implementa uma series de Rules típicas do monitoramento de clustes k8s, então eu as incrementei com alguns alertas direto do dashboard criado no grafana. Ambos enviam alertas para o Slack, foi apenas uma exemplificação de duas soluções distintas.
-   A implementação do NIC - Nginx Ingress Controller foi por conveniência. Para acessar os serviços do Cluster, basta colocar no dns local o IP público do NIC  para todas as URLs dos Ingress criados. ~ dessa forma não me preocupei com dns público ;)
-   Utilizei o Helm/Tiller em suas versões 2.13, pois a 2.14 mudou algo relativo a objetos em subcharts e esta quebrando o Chart stable/prometheus-operator

### TODO

-   CI/CD da Infraestrutura pelo Gitlab-CI
-   [Dashboards Providers](https://grafana.com/docs/installation/configuration/#provisioning);
-   Persistência do Grafana em um CloudSQL (Possibilitando HA) e utilizando o Cloud-SQL-Proxy;
-   [Integração](https://grafana.com/docs/auth/google/) 0Auth para Login com o Google Account;

Projeto criado para o processo seletivo da Getup :)

## Componentes

Esse projeto utiliza o terraform para prover uma infraestrutura como código e imutável.
Seu state está disponível aqui no repositório, mas o projeto contempla o uso de um bucket no GCP.

### Dependências

-   [JQ](https://stedolan.github.io/jq/)
-   [Terraform v11.13](https://www.terraform.io/)
-   [Helm v2.13.1](https://github.com/helm/helm/releases/tag/v2.13.1)

## Recomendações

-   [kubens & kubectx](https://github.com/ahmetb/kubectx)
-   [kubectl & gcloud](https://cloud.google.com/kubernetes-engine/docs/)

Os arquivos tf implementam os seguintes itens:

### A Nível de Projeto/Infra

-   Um Projeto na GCP (Google Cloud Platform) chamado getup-teste;
-   Uma VPC Network, com as Subnets Necessárias para a execução de um Cluster k8s nativo de VPC;
-   Um Cluster GKE (Google Kubernetes Enginer), na versão 1.13.6, um Cluster Kubernetes Gerênciado na GCP (Google Cloud Platform);
-   Um bucket para o armazenamento do terraform.tfstate
-   Ativação das APIs no GCP necessárias para o projeto em questão.

### A Nível do Cluster k8s

-   Tiller e permissões (RBAC) para a utilização do Helm
-   Nginx Ingress Controller (External Load Balancer)
-   Prometheus Operator, para a implementação do Prometheus, o Alertmanager e o Grafana

### Disclaimers

-   O GKE tem a disposição Clusters Regionais, altamente recomendados para um Cluster k8s altamente disponível, mas não o fiz para não gerar um consumo alto de créditos;
-   O GKE faz, por default, uso da tool [prometheus-to-sd](https://github.com/GoogleCloudPlatform/k8s-stackdriver/tree/master/prometheus-to-sd) desde o GKE 1.12, com a finalidade de exportar dados para o Stackdrivers e não ser necessária a instalação do Prometheus Server. Desabilitei esse recurso dada a finalidade desta avaliação :)
-   Prometheus Operator implementa uma series de Rules típicas do monitoramento de clustes k8s, então eu as incrementei com alguns alertas direto do dashboard criado no grafana. Ambos enviam alertas para o Slack, foi apenas uma exemplificação de duas soluções distintas.
-   A implementação do NIC - Nginx Ingress Controller foi por conveniência. Para acessar os serviços do Cluster, basta colocar no dns local o IP público do NIC  para todas as URLs dos Ingress criados. ~ dessa forma não me preocupei com dns público ;)
-   Utilizei o Helm/Tiller em suas versões 2.13, pois a 2.14 mudou algo relativo a objetos em subcharts e esta quebrando o Chart stable/prometheus-operator

### TODO

-   [x]   CI/CD da Infraestrutura pelo Gitlab-CI
-   [ ]   [Dashboards Providers](https://grafana.com/docs/installation/configuration/#provisioning);
-   [ ]   Persistência do Grafana em um CloudSQL (Possibilitando HA) e utilizando o Cloud-SQL-Proxy;
-   [ ]   [Integração](https://grafana.com/docs/auth/google/) 0Auth para Login com o Google Account;
