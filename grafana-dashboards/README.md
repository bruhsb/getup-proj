# POC - Getup Cloud: Dashboard Grafana

O arquivo `k8s-monitor.json` implementa uma dashboard que permite a visualização das métricas do cluster GKE criado para o Projeto em questão.

Nele é possível:

### Visualizar Métricas Gerais do Cluster:

![grafana01](/images/grafana01.png)

### Métricas Especificas quanto aos Nodes:

![grafana04](/images/grafana02.png)

### Métricas Especificas quanto aos PODs:

![grafana02](/images/grafana04.png)

### Acompanhar alertas quanto as métricas em exibição (Uso alternativo ao Alertmanager)

![grafana03](/images/grafana03.png)

O Dashboard em questão já está disponível no Grafana do Projeto. O Login de acesso ao grafana está no secret `prom-op-grafana`