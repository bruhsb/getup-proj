resource "google_storage_bucket" "terraform_state" {
    bucket_policy_only = false
    force_destroy      = false

    labels             = {
        "provisioner" = "terraform"
    }

    location           = "US-EAST1"
    name               = "terraform-state-getup-teste"
    project            = "getup-teste"
    requester_pays     = false
    storage_class      = "STANDARD"

    versioning {
        enabled = true
    }
}