#########################################
#### GKE - Google Kubernetes Enginer ####
#########################################

module "kubernetes" {
  source = "github.com/terraform-google-modules/terraform-google-kubernetes-engine?ref=v4.0.0"
  providers = {
    google = "google"
  }

  project_id                        = "getup-teste" #"${data.terraform_remote_state.gcp_project.gcp_project_id}"
  name                              = "getup-workloads"
  regional                          = "false"
  region                            = "us-east1"
  zones                             = ["us-east1-c"]
  network_project_id                = "getup-teste" #"${data.terraform_remote_state.gcp_project.gcp_project_id}"
  kubernetes_version                = "1.13.7-gke.8"
  network                           = "getup-teste" #"${data.terraform_remote_state.gcp_project.gcp_vpc_network_name}"
  subnetwork                        = "getup-teste-subnetwork" #"${data.terraform_remote_state.gcp_project.gcp_vpc_subnetwork_name}"
  ip_range_pods                     = "subnet-pods"
  ip_range_services                 = "subnet-services"
  http_load_balancing               = false
  horizontal_pod_autoscaling        = true
  kubernetes_dashboard              = false
  network_policy                    = true
  remove_default_node_pool          = false
  # issue_client_certificate          = true
  disable_legacy_metadata_endpoints = true
  monitoring_service                = "none"
  logging_service                   = "none"

  master_authorized_networks_config = [{
    cidr_blocks = [{
      cidr_block   = "0.0.0.0/0"
      display_name = "ALL"
    }]
  }]

node_pools = [
    {
      name               = "stable"
      machine_type       = "n1-standard-1"
      min_count          = 2
      max_count          = 6
      disk_size_gb       = 30
      disk_type          = "pd-standard"
      image_type         = "UBUNTU"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    stable = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

  node_pools_labels = {
    all    = {}
    stable = {}
  }

  node_pools_taints = {
    all    = []
    stable = []
  }

  node_pools_metadata = {
    all    = {}
    stable = {}
  }

  node_pools_tags = {
    all = [
      "getup-teste",
      "kubernetes",
      "default-allow-ssh",
    ]

    stable = []
  }
}

module "tiller" {
  source = "/home/brunobsbrasil/Git/terraform-tiller"
  # version = "~> 0.1.0"
  rbac_enabled  = "1"
  tiller_version = "v2.13.1"
  depends_module = "module.kubernetes"
}
