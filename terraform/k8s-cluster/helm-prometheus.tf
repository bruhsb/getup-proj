# data "template_file" "prometheus" {
#   template = "${file("${path.module}/helmfiles/prometheus.tpl.yaml")}"
#
#   vars {
#     ssl_certificate_secret = ""
#     alertmanager_url       = "alertmanager.getupproj"
#     prometheus_url         = "prometheus.getupproj"
#     grafana_url            = "grafana.getupproj"
#     WEBHOOK_URL            = "https://hooks.slack.com/services/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"
#   }
# }
#
# resource "helm_release" "prometheus" {
#   name          = "prom-op"
#   namespace     = "monitoring"
#   chart         = "stable/prometheus-operator"
#   version       = "5.11.1"
#   reuse         = true
#   reuse_values  = true
#   recreate_pods = true
#   force_update  = true
#   depends_on    = ["module.kubernetes", "module.tiller"]
#   values = [
#     "${data.template_file.prometheus.rendered}",
#   ]
#   depends_on    = ["google_project_services.apis", "google_project.getup-teste"]
#
# }
