resource "helm_release" "external_lb" {
  name          = "external-lb"
  namespace     = "ingress"
  chart         = "stable/nginx-ingress"
  version       = "1.6.16"
  reuse_values  = true
  recreate_pods = true
  force_update  = true
  depends_on    = ["module.kubernetes", "module.tiller"]

  values = [
    "${file("./helmfiles/nginx.yaml")}",
  ]

  set {
    name  = "controller.ingressClass"
    value = "external"
  }

  set {
    name  = "controller.service.annotations.cloud\\.google\\.com/load-balancer-type"
    value = "external"
  }

  set {
    name  = "controller.publishService.pathOverride"
    value = "ingress/external-lb-nginx-ingress-controller"
  }

}