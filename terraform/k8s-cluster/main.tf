# *Main Providers and pre-requisites
data "google_client_config" "default" {}

terraform {
  # required_version = "~> 0.11.13"

  backend "gcs" {
    bucket = "terraform-state-getup-teste"
    prefix = "getup-teste/k8s-cluster"
  }
}

data "terraform_remote_state" "gcp_project" {
  backend = "gcs"
  config = {
    bucket = "terraform-state-getup-teste"
    prefix = "getup-teste"
  }
}

provider "google-beta" {
  project = "getup-teste" # "${data.terraform_remote_state.gcp_project.gcp_project_id}"
  region  = "us-east1" # "${data.terraform_remote_state.gcp_project.gcp_region}"
  zone    = "us-east1-c" # "${data.terraform_remote_state.gcp_project.gcp_zone}"
  version = "~> 2.11.0"
}

provider "google" {
  project = "getup-teste" # "${data.terraform_remote_state.gcp_project.gcp_project_id}"
  region  = "us-east1" # "${data.terraform_remote_state.gcp_project.gcp_region}"
  zone    = "us-east1-c" # "${data.terraform_remote_state.gcp_project.gcp_zone}"
  version = "~> 2.11.0"
}

provider "kubernetes" {

  load_config_file = false
  host = "https://${module.kubernetes.endpoint}"
  cluster_ca_certificate = "${base64decode(module.kubernetes.ca_certificate)}"
  token = "${data.google_client_config.default.access_token}"

}

provider "helm" {

  install_tiller = false
  service_account = "tiller"
  namespace = "kube-system"

  kubernetes {

    load_config_file = false
    host = "https://${module.kubernetes.endpoint}"
    cluster_ca_certificate = "${base64decode(module.kubernetes.ca_certificate)}"
    token = "${data.google_client_config.default.access_token}"

  }
}
