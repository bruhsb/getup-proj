/******************************************
   Projects
 *****************************************/

resource "google_project" "getup-teste" {
  name                = "Getup Proj"
  project_id          = var.gcp_project
  billing_account     = var.billing_account_id
  auto_create_network = true
}

/******************************************
   VPC - Network
 *****************************************/

resource "google_compute_network" "getup-teste" {
  name                    = "getup-teste"
  description             = "General VPC Network"
  auto_create_subnetworks = "false"
  depends_on = [
    google_project_services.apis,
    google_project.getup-teste,
  ]
}

resource "google_compute_subnetwork" "getup-teste-subnetwork" {
  name          = "getup-teste-subnetwork"
  ip_cidr_range = var.vpc_ip_cidr_ranges[0]
  region        = var.region
  network       = google_compute_network.getup-teste.self_link
  secondary_ip_range {
    range_name    = "subnet-pods"
    ip_cidr_range = var.vpc_ip_cidr_ranges[1]
  }
  secondary_ip_range {
    range_name    = "subnet-services"
    ip_cidr_range = var.vpc_ip_cidr_ranges[2]
  }
  depends_on = [
    google_project_services.apis,
    google_project.getup-teste,
  ]
}

/******************************************
   APIs - Services for GCP
 *****************************************/
resource "google_project_services" "apis" {
  services = [
    "oslogin.googleapis.com",
    "iam.googleapis.com",
    "bigquery-json.googleapis.com",
    "containerregistry.googleapis.com",
    "pubsub.googleapis.com",
    "compute.googleapis.com",
    "iamcredentials.googleapis.com",
    "container.googleapis.com",
    "storage-api.googleapis.com",
    "serviceusage.googleapis.com",
    "cloudresourcemanager.googleapis.com",
  ]
}

