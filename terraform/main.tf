terraform {
  # required_version = "~> 0.11.14"

  backend "gcs" {
    bucket = "terraform-state-getup-teste"
    prefix = "getup-teste"
  }
}

provider "google-beta" {
  project = var.gcp_project
  region  = var.region
  zone    = var.zone
  version = "~> 2.11.0"
}

provider "google" {
  project = var.gcp_project
  region  = var.region
  zone    = var.zone
  version = "~> 2.11.0"
}

