# ##########################
# ### GENERAL  VARIABLES ###
# ##########################

variable "gcp_project" {
  type        = string
  description = "GCP project id that will be used"
  default     = "getup-teste"
}

variable "bucket_terraform_state" {
  type    = string
  default = "terraform-state-getup-teste"
}

#
variable "billing_account_id" {
  type        = string
  description = "Billing account used for payment in this project"
  default     = "018AF7-C81CA2-976611"
}

#
variable "region" {
  type        = string
  description = "GCP region, e.g. southamerica-east1"
  default     = "us-east1"
}

variable "zone" {
  type        = string
  description = "GCP zone, e.g. us-east1-b (which must be in gcp_region)"
  default     = "us-east1-c"
}

# ##########################
# ###       NETWORK      ###
# ##########################
variable "vpc_ip_cidr_ranges" {
  type        = list(string)
  description = "List with three ranges for vpc subnetwork and its secondary cidrs"

  default = [
    "10.100.240.0/20",
    "10.101.240.0/20",
    "10.102.240.0/20",
    "10.103.240.0/28",
  ]
}

