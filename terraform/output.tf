#################
#### OUTPUTS ####
#################

output "gcp_project_id" {
  description = "Google Cloud Project ID"
  value       = var.gcp_project
}

output "gcp_region" {
  description = "Google Cloud Project ID"
  value       = var.region
}

output "gcp_zone" {
  description = "Google Cloud Project ID"
  value       = var.zone
}

output "gcp_vpc_network_name" {
  description = "Google Cloud Project ID"
  value       = google_compute_network.getup-teste.name
}

output "gcp_vpc_subnetwork_name" {
  description = "Google Cloud Project ID"
  value       = google_compute_subnetwork.getup-teste-subnetwork.name
}

