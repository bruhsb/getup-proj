# POC - Getup Cloud: Implementação do Terraform

Os arquivos `*.tf` representam, como código, a infraestrutura e são utilizados pelo terraform para implementar toda a estrutura de recursos necessários.

## State do Terraform
 A representação do estado atual desse recursos se faz na forma do arquivo `terraform.tfstate`. Ele fica em um bucket dentro do proj, permitindo que multiplos usuários façam a sua edição, assim como a utilização do gitlab-ci.

 ![terraform01](/images/terraform01.png)

## Projeto no GCP
 Foi criado um projeto, chamado `getup-teste` para hospedar todos os recursos necessários.

 ![terraform02](/images/terraform02.png)

## Rede VPC
 Dada a necessidade do Cluster GKE precisar de dois Ranges de IP, para os PODs e para os Services, foi criada uma VPC com os atributos necessários.

  ![terraform03](/images/terraform03.png)

## Nginx Ingress Controller & External LoadBalancer
 Visando evitar a necessidade de criar um DNS público, para acesso aos IPs do Cluster, foi implementado um Ingress Controller para que todos os Ingress criados pelos recursos utilizem o mesmo IP público. 
 
  ![terraform04](/images/terraform04.png)

 
 Bastando apenas colocar no arquivo `/etc/hosts` as seguintes informações:

 ```bash
 35.237.5.156	grafana.getupproj alertmanager.getupproj prometheus.getupproj

 ```

 ## Cluster GKE

 Foi criado um cluster, não regional, para a implantação das soluções indicadas na POC. Com um autoscale de 1 a 5 nodes, o cluster conta com instâncias `n1-standard-1`.

  ![terraform05](/images/terraform05.png)
  ![terraform06](/images/terraform06.png)

O Projeto irá implementar o Prometheus Operator no namespace `monitoring` e o Nginx Ingress Controller no namespace`ingress`

  ![terraform07](/images/terraform07.png)

O Nginx Controller utiliza um IP público fixo que será o mesmo para todos os Ingress criados, tendo ele como referência. 
É curioso observar que ele faz isso por meio de um `service` tipo LoadBalancer, para criar o External LoadBalancer citado anteriormente.

  ![terraform08](/images/terraform08.png)
